from flask import Flask, request, render_template 

app = Flask(__name__)

@app.route('/', methods=["GET", "POST"])
def rootpage():
    name = ""
    
    if request.method == "POST" and 'username' in request.form:
        name = request.form.get("username")
        print(request.form)
    return render_template("index.html", 
        name=name)

@app.route('/bmiCalc', methods=["GET", "POST"])
def bmipage():
    bmi = 0
    error = None
    
    if request.method == "POST" and 'height' in request.form and 'mass' in request.form:
        if request.form.get("mass") != "" and request.form.get("height") != "" :  
            mass = int(request.form.get("mass"))
            height = int(request.form.get("height"))
            bmi = round(mass/(height/100)**2, 2)
            error = None
        
        else:
            error = "Did you pass weight and height?"

    return render_template("bmiCalc.html", 
        bmi=bmi, error=error)



app.run()
