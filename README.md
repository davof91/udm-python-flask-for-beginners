# Udemy - Python Flask for Beginners

Python Flask for Beginners is a course that will teach you how to write your first Python Flask web application. No fluff, just code. (https://www.udemy.com/course/python-flask-for-beginners/) 

## What can you find in the repo
The repo contains the code that is build throughout the class and the code for the final project all in the same files. To try it out run "python app.py". This wiill initialize the Flask app on localhost.

## Before running
Install Flask using pip by running "pip install flask"